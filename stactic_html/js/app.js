$(document).ready(function() {
    $(".item-input").focus();
    $('.item-input').on('keyup',function(e){
          var input = $(this);
          var lengthNumber = input.val().length;
          input.next(".forcus-list .forcus-item").text(lengthNumber + " chars");
          $('.forcus-list .forcus-item:nth-child(' + lengthNumber +')').addClass('active');
          if(e.keyCode == 46 || e.keyCode == 8) {
                $('.forcus-list .forcus-item:nth-child(' + (lengthNumber + 1) +')').removeClass('active');
            }
    });

    
	var listBanners = $('.pro-track');
	
	for(var i = 0; i < listBanners.length; i++) {
		
		var totalWidth = 0;
		$(listBanners[i]).find('.pro-item').each(function() {
			totalWidth += parseInt($(this).width(), 10);	
		});
		
		var v = totalWidth*1.35;

		$(listBanners[i]).width(v);
	}
   
    globalFontResize();
    getheight_sizer();
    $(window).resize(globalFontResize);
    $(window).resize(getheight_sizer);
});

function globalFontResize(){
    w = $(window).width();

    var vw = w / 1440 * 65;
    jQuery('html').attr('style', 'font-size: ' + vw  + 'px');
    jQuery('body').css({
        'visibility' : 'visible'
    });

}
function getheight_sizer() {
    var wi = $('.card').width();
    $('.card').height(wi/1.5);
    
}